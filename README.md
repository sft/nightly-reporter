Nightly reporter
================

A python script that generates [a static HTML](https://lcgdocs.web.cern.ch/lcgdocs/report/) page based on the Jenkins API.
This page displays the current state of the LCG nightly build (without history like in CDash).
The script is called via a [cronjob](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_nightly_reporter/) inside Jenkins.


## Authentication

In order to authenticate via the Kerberos interface of the CERN single sign-on system, you need to run
the script as user `sftnight` on a proper CERN-provided VM or Docker image that has access to the correct 
Kerberos configuration as well as a ticket file.

```
kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
```

In order to access the the Jenkins API without the Single Sign-On interface, you can create a SSH tunnel
before running the script. Thats's why the script accesses the API via `http://localhost:8080`

```
ssh -f -N -L 8080:localhost:16820 sftnight@epsft-jenkins
```


## Start script

All dependencies and the correct CET timezone are prepared in a Docker image that can be used in order to execute the script locally 
without installing the dependencies:

```
docker run -it --rm --net="host" \
    -v "${PWD}":/opt/reporter \
    gitlab-registry.cern.ch/sft/nightly-reporter:latest \
    python main.py
```

After the build script has run successfully you can deploy the entire `report` folder to a web
server of your choice. For sake of simplicity we chose to use the same web server as `lcgdocs`
that serves the `/eos/project/l/lcgdocs/www/` directory on the EOS.


## Provide a new Docker image

The Docker images are stored in the GitLab registry. You can update the image like this:

```
docker login gitlab-registry.cern.ch
docker build -t gitlab-registry.cern.ch/sft/nightly-reporter:latest .
docker push gitlab-registry.cern.ch/sft/nightly-reporter:latest
```


## Cleaning the repository

To remove all build artifacts in the local repository such as `*.pyc` files and `dist/report.html`, you
can run the following command as long as there are no other unstaged files in git. Be careful not to
delete more than necessary.

```
git clean -xi
```


## Icons

The icons are taken from the CERN Mattermost chat. These links serve as documentation of the original sources:

 - [`img/error.png`](https://mattermost.web.cern.ch/static/emoji/1f6ab.png)
 - [`img/failure.png`](https://mattermost.web.cern.ch/static/emoji/1f4db.png)
 - [`img/running.png`](https://mattermost.web.cern.ch/static/emoji/25b6-fe0f.png)
 - [`img/scheduled.png`](https://mattermost.web.cern.ch/static/emoji/23f3.png)
 - [`img/success.png`](https://mattermost.web.cern.ch/static/emoji/2705.png)
 - [`img/unstable.png`](https://mattermost.web.cern.ch/static/emoji/26a0-fe0f.png)

