from http.cookiejar import MozillaCookieJar
from pprint import pprint, pformat
import codecs
import copy
import datetime
import logging
import os
import re
import sys
import xml.dom.minidom
from collections import defaultdict, OrderedDict
from time import sleep
from typing import Optional, Sequence, AnyStr, MutableMapping, Dict, Pattern, Union, Callable
from urllib.parse import urlparse
from xml.parsers.expat import ExpatError

import dateutil.parser
import dateutil.tz
import requests
import simplejson

from html_engine import HTML


SERVER_GLOBAL = "https://lcgapp-services.cern.ch/spi-jenkins"
SERVER = "http://localhost:8080/spi-jenkins"
CVMFS_REPOSITORY = "/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/"

STABLE_PREFIX = "isDone-"
UNSTABLE_PREFIX = "isDone-unstable-"

MAX_RETRIES = 10  # Maximum number of retries before giving up
RETRY_TIMEOUT = 15  # Time (s) between retries

session: Optional[requests.Session] = None
logger: Optional[logging.Logger] = None
find_logger: Optional[logging.Logger] = None
parser_logger: Optional[logging.Logger] = None
old_factory: Optional[Callable] = None
log_lineno = None

ts_regex = re.compile(r'\[\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)\]')


def record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)  # type: logging.LogRecord
    if record.name == 'parser':
        record.log_lineno = log_lineno
    return record


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()

    raise TypeError("Type %s not serializable" % type(obj))


def configure_logger(lg: logging.Logger, level, filename):
    lg.setLevel(logging.DEBUG)

    if lg.name == 'parser':
        fmt = "%(asctime)s.%(msecs)03d [%(name)s:%(levelname)s:%(filename)s@%(lineno)s] L%(log_lineno)s: %(message)s"
        fmt_c = "%(asctime)s.%(msecs)03d% (log_color)s[%(name)s:%(levelname)s:%(filename)s@%(lineno)s]%(reset)s " \
                "L%(log_lineno)s: %(message)s"
    else:
        fmt = "%(asctime)s.%(msecs)03d [%(name)s:%(levelname)s:%(filename)s@%(lineno)s] %(message)s"
        fmt_c = "%(asctime)s.%(msecs)03d %(log_color)s[%(name)s:%(levelname)s:%(filename)s@%(lineno)s]%(reset)s " \
                "%(message)s"

    # add the handlers to the logger
    ch1 = logging.StreamHandler(sys.stdout)
    ch1.setLevel(level)
    ch1.setFormatter(logging.Formatter(fmt=fmt, datefmt='%H:%M:%S'))
    lg.addHandler(ch1)

    if filename is not None:
        ch2 = logging.FileHandler(filename)
        ch2.setLevel(logging.DEBUG)
        ch2.setFormatter(logging.Formatter(fmt=fmt, datefmt='%H:%M:%S'))
        lg.addHandler(ch2)


def setup_logging(level_main, level_finder, level_parser, log_filename):
    global logger, parser_logger, find_logger, old_factory

    old_factory = logging.getLogRecordFactory()
    logging.setLogRecordFactory(record_factory)

    logger = logging.getLogger('main')
    configure_logger(logger, level_main, log_filename)

    parser_logger = logging.getLogger('parser')
    configure_logger(parser_logger, level_parser, log_filename)

    find_logger = logging.getLogger('finder')
    configure_logger(find_logger, level_finder, log_filename)


def parse_pipeline_log(lines):
    global log_lineno
    stage_name_list = []
    is_stage = False
    should_append = True
    should_remove = False
    is_log = False
    messages = OrderedDict()
    for lineno, line in enumerate(lines):
        log_lineno = lineno + 1
        line = line.strip('\n')
        if not (ts_regex.match(line) or line.startswith('[Pipeline')):
            continue

        tag, text = line.split(' ', 1)
        tag = tag.strip('[]')

        if tag == 'Pipeline':
            if text in ('Start of Pipeline', 'End of Pipeline'):
                continue

            if (not text.startswith('//')) and should_remove:
                parser_logger.critical('shold_remove set but line doesn\'t start with "//"')
                return {}

            is_log = False

            if text == 'stage':
                if is_stage:
                    parser_logger.critical('ERROR: Got "stage", was expecting stage name!')
                    return {}

                is_stage = True
                parser_logger.debug('Got "stage", setting flag is_stage...')
                continue

            if text.startswith('{'):
                if is_stage:
                    try:
                        stage_name = text.split(' ', 1)[1]
                        is_stage = False
                        should_append = True
                        stage_name_list.append(stage_name)
                        parser_logger.debug(f'Got stage name "{stage_name}", resetting is_stage flag')
                    except IndexError:
                        parser_logger.critical('Stage name not found!')
                        return {}
                else:
                    parser_logger.debug('Got <begin>, setting should_append flag...')
                    should_append = True
            elif text == '}':
                parser_logger.debug('Got <end>, setting should_remove flag...')
                should_remove = True
            elif text.startswith('//'):
                if not should_remove:
                    parser_logger.critical(f'Got {text}, but flag not set')
                    return {}
                else:
                    try:
                        text = text.split(' ', 1)[1]
                    except IndexError:
                        parser_logger.critical('Got <section_end> but no name')
                        return {}

                    while True:
                        try:
                            last_name = stage_name_list.pop().split('#', 1)[0]
                        except IndexError:
                            parser_logger.critical('stage_name_list is empty while procesing <section_end>')
                            return {}

                        if last_name == text or (
                                text == 'stage' and last_name.startswith('(') and last_name.endswith(')')):
                            parser_logger.debug(f'popped "{last_name}" as expected')
                            should_remove = False
                            should_append = True
                            break
                        parser_logger.debug(f'popped "{last_name}", expecting "{text}"')
            else:
                if should_append:
                    stage_name_list.append(text)
                    should_append = False
                    parser_logger.debug(f'Got step name "{text}", resetting flag')
                else:
                    old_stage = 'UNKNOWN'
                    try:
                        old_stage = stage_name_list.pop()
                    except IndexError:
                        parser_logger.warning(f'Step list empty when trying to set step name to "{text}"!')
                    finally:
                        stage_name_list.append(text)
                        parser_logger.debug(f'Got step name "{text}", replacing prev step "{old_stage}"')
        else:
            if not stage_name_list:
                parser_logger.critical('Step list empty when assigning message to step!')
                return {}

            stage_name = '.'.join(stage_name_list)
            if not is_log:
                # Start of new log block
                if stage_name in messages:
                    index = 0
                    last_stage = stage_name_list.pop()
                    # if '#' in last_stage
                    while True:
                        stage_name_list.append(last_stage + '#' + str(index))
                        stage_name = '.'.join(stage_name_list)
                        if stage_name not in messages:
                            break
                        index += 1
                        stage_name_list.pop()

            is_log = True
            timestamp = dateutil.parser.parse(tag).astimezone(dateutil.tz.tzlocal())
            if stage_name not in messages:
                messages[stage_name] = []
            messages[stage_name].append((timestamp, text))
            # my_print(f'DEBUG: got message in stage {stage_name}, timestamp {tag} =
            # {dateutil.parser.parse(tag).astimezone(dateutil.tz.tzlocal()).strftime("%X")}')

    return messages

def parse_pipeline_info(pipeLineInfoList):
    stage_times = {}

    stage_map = OrderedDict(
        Build='build',
        CVMFSInstall='install',
        TestWithViews='test',
        TestPythonImport='testi',
        TestShell='testsh'
    )

    for stageInfo in pipeLineInfoList["stages"]:
        stageName = stageInfo["name"]
        status = stageInfo["status"]
        if stageName in stage_map and status not in ("NOT_EXECUTED", ):
            parser_logger.debug(f"Info for {stageName}: {pformat(stageInfo)}")
            time_start = datetime.datetime.fromtimestamp(stageInfo["startTimeMillis"]/1000)
            pauseD = stageInfo["pauseDurationMillis"]
            stageD = stageInfo["durationMillis"]
            time_elapsed = datetime.timedelta(milliseconds=stageD-pauseD)
            stage_times[stage_map[stageName]] = {'timestamp': time_start,
                                                 'duration': time_elapsed}

    return stage_times

def analyze_pipeline_log(messages, stage_style='docker'):
    parser_logger.info("Analyzing pipeline log")

    stage_map = OrderedDict(
        build='(Build)',
        install='(CVMFSInstall)',
        test='(TestWithViews)',
        testi='(TestPythonImport)',
        testsh='(TestShell)'
    )

    res = {}
    # with open('messages.json', 'w') as f:
    #     simplejson.dump(messages, f, indent=4, default=json_serial)
    #     exit(0)
    for stage_name, stage_id in stage_map.items():
        theContent = None
        for message, content in messages.items():
            if stage_id in message and message.endswith(".sh"):
                parser_logger.info(f"Found stage with {stage_id}: {message}")
                theContent = content
                break
        if not theContent:
            parser_logger.warning(f"Stage {stage_id} not found")
            continue
        # noinspection PyShadowingNames
        time_start = theContent[0][0]
        time_start_s = time_start.strftime('%X')
        time_end = theContent[-1][0]
        time_end_s = time_end.strftime('%X')
        time_elapsed = time_end - time_start
        parser_logger.debug(f"Stage {stage_name} started at {time_start_s} and finished at {time_end_s},"
                            f" took {time_elapsed}")
        res[stage_name] = {'timestamp': time_start, 'duration': time_elapsed}

    return res


def encode_datetime(obj):
    if isinstance(obj, datetime.datetime):
        return obj.strftime('%X')
    raise TypeError(repr(obj) + " is not JSON serializable")


def init():
    global session, SERVER
    SERVER = SERVER_GLOBAL
    session = requests.Session()
    # cookies.txt was created by auth-get-sso-cookies
    cookies = MozillaCookieJar('/.local/cookies.txt')
    cookies.load()
    session.cookies = cookies


def get(url, doRetry=True):
    url = url.replace(SERVER_GLOBAL, SERVER)
    tryOnceMore = True
    retry = 1
    while retry <= MAX_RETRIES and tryOnceMore:
        try:
            r = session.get(url)
            r.raise_for_status()
        except requests.RequestException as e:
            logger.error("Request for {0} failed: {1}, {2}".format(url, e,
                                                                   "retrying" if (doRetry and retry != MAX_RETRIES) else "aborting!"))
            retry += 1
            if doRetry:
                sleep(RETRY_TIMEOUT)
            else:
                tryOnceMore = False
        else:
            return r
    if not doRetry:
        return r
    logging.fatal("Failed to retrieve {0} in {1} tries".format(url, MAX_RETRIES))
    exit(1)


def parse_combinations(s):
    res = []
    for comb in s.split('||'):
        comb = comb.strip().strip('()')
        partd = {}
        for part in comb.split('&&'):
            part = part.strip()
            key, value = map(lambda x: x.strip().replace("'", "").replace('"', ''), part.split('=='))
            partd[key] = value

        res.append(partd)

    return res


def parse_build_url(url):
    parts = url.strip('/').rsplit('/', 2)[1]
    comb_ = parts
    comb = {}
    for c in comb_.split(','):
        k, v = c.split('=')
        comb[k] = v

    return comb, comb_


def parse_log(log: Sequence[AnyStr], expressions: Dict[AnyStr, Union[Pattern, AnyStr]]) -> Dict[AnyStr, Sequence]:
    matches = defaultdict(list)
    patterns: MutableMapping[AnyStr, Pattern] = copy.deepcopy(expressions)
    for key, exp in expressions.items():
        if not isinstance(exp, Pattern):
            patterns[key] = re.compile(exp)
        else:
            patterns[key] = exp

    for line in log:
        for key, pat in patterns.items():
            matches[key].extend(pat.findall(line))

    return matches


def parse_build_log(log, **kwargs):
    def parse_qs(qs):
        parts = qs.split('&')
        res_ = dict(x.split('=', 1) for x in parts)
        return res_

    n_build = {}

    log_patterns = {'errors': r'(\d+) Compiler errors',
                    'warnings': r'(\d+) Compiler warnings',
                    'buildid':  r'<buildId>(\d+)</buildId>',
                    'hostname': r'Building on (.*) in',
                    }
    log_patterns.update(kwargs)

    matches = parse_log(log, log_patterns)

    if matches['errors']:
        n_build['compiler_errors'] = int(matches['errors'][0])

    if matches['warnings']:
        n_build['compiler_warnings'] = int(matches['warnings'][0])

    if matches['buildid']:
        n_build['build_id'] = int(matches['buildid'][0])

    if len(matches['hostname']) > 1:
        # the first (0) match is 'startup-jobs', the second for the build machine
        n_build['hostname'] = matches['hostname'][1]

    for k in kwargs:
        if matches[k] and k not in n_build:
            n_build[k] = matches[k][0]

    return n_build


def build_platform(comb):
    res = '%s-' % comb.get("HOST_ARCH", "x86_64")
    if 'ARCHITECTURE' in comb:
        res = 'x86_64+' + comb['ARCHITECTURE'] + '-'
    os_name = comb['LABEL'].replace('lcg_docker_', '').replace('_noafs', '')
    if os_name == 'ubuntu18':
        os_name = 'ubuntu1804'

    if os_name == 'ubuntu16':
        os_name = 'ubuntu1604'

    if os_name == 'ubuntu20':
        os_name = 'ubuntu2004'

    if os_name == 'ubuntu22':
        os_name = 'ubuntu2204'

    if os_name in 'cc7':
        os_name = 'centos7'

    if os_name in 'cc8':
        os_name = 'centos8'

    res += os_name + '-'
    compiler_name = comb['COMPILER'].replace('binutils', '').replace('libcpp', '')
    if compiler_name == 'clang600':
        compiler_name = 'clang60'

    if compiler_name == 'clang800':
        compiler_name = 'clang8'

    if compiler_name == 'clang900':
        compiler_name = 'clang9'

    if compiler_name == 'native':
        if os_name == 'ubuntu1604':
            compiler_name = 'gcc54'
        elif os_name == 'centos7':
            compiler_name = 'gcc48'
        elif os_name == 'ubuntu1804':
            compiler_name = 'gcc7'
        elif os_name == 'ubuntu2004':
            compiler_name = 'gcc9'
        elif os_name == 'ubuntu2204':
            compiler_name = 'gcc11'

    res += compiler_name + '-' + ('opt' if comb['BUILDTYPE'] == 'Release' else 'dbg')
    return res


def build_platform_simple(comb):
    return ','.join('{0}={1}'.format(k, v) for k, v in comb.items())


def get_builds(job_name, start, count=10):
    r = get(SERVER + '/job/{0}/api/json?tree=builds[number,url]{{{1},{2}}}'.format(job_name, start, start + count))
    r.raise_for_status()

    return r.json()


def get_build(job_name, build_no, depth=1):
    r = get(SERVER + '/job/{0}/{1}/api/json?depth={2}'.format(job_name, build_no, depth))
    r.raise_for_status()

    text = r.text.replace('\x1b', '')

    try:
        return simplejson.loads(text)
    except simplejson.JSONDecodeError:
        f = codecs.open("job_{0}_build_{1}_depth_{2}.txt".format(job_name, build_no, depth), 'w', encoding='utf8')
        f.write(r.text)
        logging.error("JSON parsing failed!")
        logging.debug("Data saved to", "job_{0}_build_{1}_depth_{2}.txt".format(job_name, build_no, depth))
        f.close()
        return None


def parse_build(build):
    build_class = build.get('_class', 'hudson.matrix.MatrixRun')

    if build_class in ('hudson.matrix.MatrixBuild', 'hudson.matrix.MatrixRun',
                       'hudson.model.FreeStyleBuild') \
            or build_class.startswith('hudson.model.Queue$'):
        return parse_matrix_build(build)
    elif build_class == 'org.jenkinsci.plugins.workflow.job.WorkflowRun':
        return parse_pipeline_build(build)
    else:
        logger.error(f"Unknown build type {build_class}!")
        return {}


def get_absolute_url(url):
    if url.startswith('http'):
        return url
    else:
        return SERVER_GLOBAL + '/' + url.lstrip('/')


def parse_matrix_build(build):
    build_data = {'build_id': None, 'compiler_errors': None, 'compiler_warnings': None}

    def parse_parameters(data):
        res = {}
        for p in data.get('parameters', []):
            if p['name'] in ('LCG_VERSION', 'PLATFORM'):
                res[p['name']] = p['value']
        return res

    def parse_reason(data):
        for c in data.get('causes', []):
            if c.get('_class', '') == 'hudson.model.Cause$UpstreamCause':
                return 'Upstream', c['upstreamProject'], c['upstreamBuild']
            if c.get('_class', '') == 'hudson.triggers.TimerTrigger$TimerTriggerCause':
                return 'Timer',
            if c.get('_class', '') == 'hudson.triggers.SCMTrigger$SCMTriggerCause':
                return 'SCM',
        return 'Other',

    for a in build.get('actions', []):
        if a.get('_class', '') == 'hudson.model.ParametersAction':
            build_data['params'] = parse_parameters(a)

        if a.get('_class', '') == 'hudson.model.CauseAction':
            build_data['reason'] = parse_reason(a)

    if 'duration' in build:
        build_data['duration'] = build['duration'] / 1000.
    # elif 'buildableStartMilliseconds' in build:
    #     build_data['duration'] = build['buildableStartMilliseconds'] / 1000.
    else:
        build_data['duration'] = 0.

    build_data['result'] = build.get('result', 'SCHEDULED')
    if 'timestamp' in build:
        build_data['timestamp'] = datetime.datetime.fromtimestamp(float(build['timestamp']) / 1000.)
    elif 'inQueueSince' in build:
        build_data['timestamp'] = datetime.datetime.fromtimestamp(float(build['inQueueSince']) / 1000.)
    else:
        build_data['timestamp'] = datetime.datetime.now()

    build_data['hostname'] = build.get('builtOn', '')
    build_data['url'] = get_absolute_url(build['url'])
    build_data['number'] = build.get('number', -1)
    build_data['runs'] = copy.deepcopy(build.get('runs', []))

    return build_data


def parse_pipeline_build(build):
    build_data = {'build_id': None, 'compiler_errors': None, 'compiler_warnings': None}

    def parse_parameters(data):
        res = {}
        for p in data.get('parameters', []):
            if p['name'] in ('LCG_VERSION', 'PLATFORM'):
                res[p['name']] = p['value']
        return res

    def parse_reason(data):
        for c in data.get('causes', []):
            if c.get('_class', '') == 'hudson.model.Cause$UpstreamCause':
                return ['Upstream', c['upstreamProject'], c['upstreamBuild']]
            if c.get('_class', '') == 'hudson.triggers.TimerTrigger$TimerTriggerCause':
                return ['Timer', ]
            if c.get('_class', '') == 'hudson.triggers.SCMTrigger$SCMTriggerCause':
                return ['SCM', ]
        return 'Other',

    def convert_icon_color(color):
        if color.lower() == 'blue':
            return 'SUCCESS'
        elif color.lower() == 'yellow':
            return 'UNSTABLE'
        elif color.lower() == 'red':
            return 'FAILURE'
        elif 'anime' in color.lower():
            return 'RUNNING'
        else:
            return 'ABORTED'

    def parse_pipeline(data):
        res = {}
        for step in data:
            stepName = ''
            displayName = step.get('displayName', '')
            # print(">>", displayName)
            if displayName == 'build_and_test':
                stepName = 'build'
            elif displayName == 'cvmfs_install':
                stepName = 'install'
            elif displayName == 'test-with-views':
                stepName = 'test'
            elif displayName == 'test-shell':
                stepName = 'testsh'
            elif displayName == 'test-pythonimport':
                stepName = 'testi'

            if stepName:
                if step['running']:
                    step['result'] = None
                else:
                    step['result'] = convert_icon_color(step['iconColor'])

                if 'url' in step:
                    step['url'] = get_absolute_url(step['url'])
                res[stepName] = step

        return res

    for a in build.get('actions', []):
        if a.get('_class', '') == 'hudson.model.ParametersAction':
            build_data['params'] = parse_parameters(a)

        if a.get('_class', '') == 'hudson.model.CauseAction':
            build_data['reason'] = parse_reason(a)

        if a.get('_class', '') == 'org.jenkinsci.plugins.workflow.job.views.FlowGraphAction':
            if a.get('nodes', None):
                build_data['runs'] = parse_pipeline(a['nodes'])
            else:
                build_data['runs'] = {}

    if 'duration' in build:
        build_data['duration'] = build['duration'] / 1000.
    # elif 'buildableStartMilliseconds' in build:
    #     build_data['duration'] = build['buildableStartMilliseconds'] / 1000.
    else:
        build_data['duration'] = 0.

    build_data['result'] = build.get('result', 'SCHEDULED')
    if 'timestamp' in build:
        build_data['timestamp'] = datetime.datetime.fromtimestamp(float(build['timestamp']) / 1000.)
    elif 'inQueueSince' in build:
        build_data['timestamp'] = datetime.datetime.fromtimestamp(float(build['inQueueSince']) / 1000.)
    else:
        build_data['timestamp'] = datetime.datetime.now()

    build_data['hostname'] = build.get('builtOn', '')
    build_data['url'] = get_absolute_url(build['url'])
    build_data['number'] = build.get('number', -1)
    # build_data['runs'] = copy.deepcopy(build.get('runs', []))

    return build_data


def find_build(job_name, filter_f, count=10):
    start = 0
    result = None

    while True:
        r = get_builds(job_name, start, count)
        build_numbers = [b['number'] for b in r['builds']]
        if not build_numbers:
            break

        for bn in build_numbers:
            find_logger.debug("Checking {0} #{1}".format(job_name, bn))
            rr = get_build(job_name, bn, 0)
            if rr is None:
                find_logger.error("Failed to get build {0} #{1}!".format(job_name, bn))
                continue

            build = parse_build(rr)
            if build['timestamp'].date() != datetime.date.today():
                find_logger.debug("Build is from the past, skipping".format(bn))
                break

            find_logger.debug('Build cause: ' + ' '.join(map(str, build['reason'])))
            if filter_f(build):
                find_logger.debug("Build matched!")
                rr = get_build(job_name, bn, 3)
                build = parse_build(rr)
                return copy.deepcopy(build)
        else:
            start += count
            continue

        break

    # Check if the build in question is still in queue
    if result is None:
        find_logger.info("Checking build queue")
        r = get(SERVER + '/queue/api/json')
        j = r.json()
        for i, item in enumerate(j['items']):
            if item.get('task', {}).get('name', '') != job_name:
                continue
            build = parse_build(item)
            if filter_f(build):
                find_logger.info("Found build in queue, position {0}".format(i))
                return copy.deepcopy(build)
    return None


def find_build_yesterday(job_name, filter_f, count=10):
    start = 0

    while True:
        r = get_builds(job_name, start, count)
        build_numbers = [b['number'] for b in r['builds']]
        if not build_numbers:
            break

        for bn in build_numbers:
            find_logger.debug("Checking {0} #{1}".format(job_name, bn))
            rr = get_build(job_name, bn)
            if rr is None:
                find_logger.error("Failed to get build {0} #{1}!".format(job_name, bn))
                continue

            build = parse_build(rr)
            if build['timestamp'].date() < (datetime.date.today() - datetime.timedelta(hours=24)):
                find_logger.debug("Build is from the past, skipping".format(bn))
                break

            find_logger.debug('Build cause: ' + ' '.join(map(str, build['reason'])))
            if filter_f(build):
                find_logger.debug("Build matched!")
                return copy.deepcopy(build)
        else:
            start += count
            continue
        break
    return None


def do_matrix(job_name, mode='dev'):
    res = {}

    if mode == 'clean':
        latest_build = find_build_yesterday(job_name, lambda x: x['reason'][0] == 'Timer', count=1)
    else:
        latest_build = find_build(job_name, lambda x: x['reason'][0] == 'Timer', count=1)

    if latest_build is None:
        logger.error("Could not find latest build for job " + job_name)
        return None

    latest_build_no = latest_build['number']
    logger.info("Found build {0} #{1}".format(job_name, latest_build_no))

    for run in latest_build['runs']:
        n_build = parse_build(run)

        comb, mask = parse_build_url(n_build['url'])
        if mode == 'dev' or mode == 'exp' or mode == 'devgeantv':
            platform = build_platform(comb)
        else:
            platform = build_platform_simple(comb)

        if mode != 'clean' and n_build['timestamp'].date() != datetime.date.today():
            logger.warning(
                "Skipping platform {0}: build from the past {1}".format(platform, n_build['timestamp'].isoformat()))
            continue

        logger.info("Found platform " + platform)
        res[platform] = {}

        if mode != 'dev' and mode != 'devgeantv':
            res[platform]['build'] = n_build
            continue

        if n_build['result'] not in (None, 'SCHEDULED'):
            build_extra = parse_build_log(get('{0}consoleText'.format(n_build['url'])).text.split('\n'))
            n_build['build_id'] = build_extra.get('build_id', None)
            n_build['compiler_errors'] = build_extra.get('compiler_errors', -1)
            n_build['compiler_warnings'] = build_extra.get('compiler_warnings', -1)

            if n_build['compiler_errors'] == 0 and \
                    n_build['compiler_warnings'] == 0 and \
                    n_build.get('result', '') is not None and \
                    n_build.get('result', '').upper() == 'FAILURE':
                n_build['result'] = 'UNSTABLE'
        else:
            latest_build_no = -1  # Block going down

        res[platform]['build'] = n_build

        if latest_build_no < 0:
            logger.debug("Build in queue, not going down")

        upstream_project = job_name + '/' + mask

        if mode == 'dev':
            if logger.getEffectiveLevel() != logging.DEBUG:
                logger.info("Looking for downstream build of lcg_post_build")

            post_build = find_build('lcg_post_build',
                                    lambda x: x['reason'][0] == 'Upstream' and
                                              x['reason'][1] == upstream_project and
                                              x['reason'][2] == n_build['number']
                                    )
            if post_build is None:
                logger.warning("Downstream build post_build not found!")
                continue

            post_no = post_build['number']
            logger.info("Found build post_build #" + str(post_no))

            res[platform]['post_build'] = post_build

            if post_no < 0:
                logger.debug("Post-build still in queue, not going down")
                continue

            upstream_project = 'lcg_post_build/default'
        else:
            post_build = n_build

        logger.info("Looking for downstream build of lcg_cvmfs_install")
        find_logger.debug("Find downstream of {0} #{1}".format(upstream_project, post_build['number']))

        install = find_build('lcg_cvmfs_install',
                             lambda x: x['reason'][0] == 'Upstream' and
                                       x['reason'][1] == upstream_project and
                                       x['reason'][2] == post_build['number']
                             )
        if install is None:
            logger.warning("Downstream build lcg_cvmfs_install not found!")
            continue

        install_no = install['number']
        logger.info("Found build lcg_cvmfs_install #" + str(install_no))
        res[platform]['install'] = install

        logger.info("Looking for downstream build of lcg_test_archdocker")
        find_logger.debug("Find downstream of {0} #{1}".format(upstream_project, post_build['number']))

        test = find_build('lcg_test_archdocker',
                          lambda x: x['reason'][0] == 'Upstream' and
                                    x['reason'][1] == upstream_project and
                                    x['reason'][2] == post_build['number']
                          )
        if test is None:
            logger.warning("Downstream build lcg_test_archdocker not found!")
            continue

        test_no = test['number']
        logger.info("Found build lcg_test_archdocker #" + str(test_no))
        res[platform]['test'] = test

        logger.info("Looking for downstream build of lcg_testpythonimport_archdocker")
        find_logger.debug("Find downstream of {0} #{1}".format(upstream_project, post_build['number']))

        testi = find_build('lcg_testpythonimport_archdocker',
                           lambda x: x['reason'][0] == 'Upstream' and
                                     x['reason'][1] == upstream_project and
                                     x['reason'][2] == post_build['number']
                           )
        if testi is None:
            logger.warning("Downstream build lcg_testpythonimport_archdocker not found!")
            continue

        testi_no = testi['number']
        logger.info("Found build lcg_testpythonimport_archdocker #" + str(testi_no))
        res[platform]['testi'] = testi

        logger.info("Looking for downstream build of lcg_test_shell_archdocker")
        find_logger.debug("Find downstream of {0} #{1}".format(upstream_project, post_build['number']))

        testsh = find_build('lcg_test_shell_archdocker',
                            lambda x: x['reason'][0] == 'Upstream' and
                                      x['reason'][1] == upstream_project and
                                      x['reason'][2] == post_build['number']
                            )
        if testsh is None:
            logger.warning("Downstream build lcg_test_shell_archdocker not found!")
            continue

        testsh_no = testsh['number']
        logger.info("Found build lcg_test_shell_archdocker #" + str(testsh_no))
        res[platform]['testsh'] = testsh

    return res


def do_matrix_pipeline(job_name, mode, lcg_version):
    res = {}

    latest_build = find_build(job_name, lambda x: x['reason'][0] == 'Timer', count=1)

    if latest_build is None:
        logger.error("Could not find latest build for job " + job_name)
        return None

    latest_build_no = latest_build['number']
    logger.info("Found build {0} #{1}".format(job_name, latest_build_no))

    for run in latest_build['runs']:  # type: dict
        reason_url = run['url'].replace(SERVER_GLOBAL, '').replace('/job/', '').rsplit('/', 2)[0]
        run_combination = parse_build_url(reason_url)[0]
        if run_combination['LCG_VERSION'] != lcg_version:
            logger.debug(f"Skipping run {reason_url}")
            continue

        logger.debug(f"Looking for build with Upstream {reason_url} {run['number']}")
        pipeline_build = find_build('lcg_nightly_pipeline',
                                    lambda x: x['reason'][0] == 'Upstream' and
                                              x['reason'][1] == reason_url and
                                              x['reason'][2] == run['number'] and
                                              x['params']['LCG_VERSION'] == lcg_version
                                    )

        comb, mask = parse_build_url(run['url'])
        if mode == 'dev' or mode == 'exp' or mode == 'devgeantv':
            platform = build_platform(comb)
        else:
            platform = build_platform_simple(comb)

        if pipeline_build is None:
            logger.info("Skipping platform {0}: no downstream build".format(platform))
            continue

        if mode != 'clean' and pipeline_build['timestamp'].date() != datetime.date.today():
            logger.warning(
                "Skipping platform {0}: build from the past {1}".format(platform,
                                                                        pipeline_build['timestamp'].isoformat()))
            continue

        logger.info("Found platform " + platform)
        res[platform] = {}

        log = get("{0}consoleText".format(pipeline_build['url'])).text.split('\n')
        matches = parse_build_log(log,
                                  url='Upload file: .*? to ([^ ]+)',
                                  hostname=r'Running on (.*) in',
                                  test=r'(\d+)% tests passed')

        log = get("{0}wfapi/describe".format(pipeline_build['url'])).text.split('\n')
        stage_times = parse_pipeline_info(simplejson.loads(log[0]))

        startT, installT, installD, viewT, viewD = 0, 0, 0, 0, 0
        if 'install' in pipeline_build['runs']:
            nodeURL, lastNodeId = pipeline_build['runs']['install']['url'].strip('/').rsplit('/', 1)
            # this range is rather adhoc...
            for aNode in range(int(lastNodeId)-30, int(lastNodeId)+50):
                installRunLog = get(f"{nodeURL}/{aNode}/wfapi/describe", doRetry=False)
                try:
                    theLog = installRunLog.json()
                except:
                    continue
                if not theLog:
                    continue
                if theLog['name'] == 'CVMFSInstall':
                    startT = datetime.datetime.fromtimestamp(theLog["startTimeMillis"]/1000)
                    installT = datetime.timedelta(milliseconds=theLog['durationMillis'])
                    theInstall = [ent for ent in theLog['stageFlowNodes'] if ent['name'] == 'cvmfs_install']
                    if not theInstall:
                        continue
                    theInstall = theInstall[0]
                    installD = datetime.timedelta(milliseconds=theInstall['durationMillis']-theInstall['pauseDurationMillis'])
                if theLog['name'] == 'ViewCreation':
                    startT = datetime.datetime.fromtimestamp(theLog["startTimeMillis"]/1000)
                    viewT = datetime.timedelta(milliseconds=theLog['durationMillis'])
                    theInstall = [ent for ent in theLog['stageFlowNodes'] if ent['name'] == 'cvmfs_view_creation']
                    if not theInstall:
                        continue
                    theInstall = theInstall[0]
                    viewD = datetime.timedelta(milliseconds=theInstall['durationMillis']-theInstall['pauseDurationMillis'])
                    break # if we are here, we found all the times we want

        for k in copy.copy(tuple(pipeline_build['runs'].keys())):
            res[platform][k] = pipeline_build['runs'][k]
            res[platform][k]['duration'] = stage_times.get(k, {}).get('duration', 0)
            res[platform][k]['timestamp'] = stage_times.get(k, {}).get('timestamp', 0)

        # for cases where we do not create the view, we only have the install parts
        if startT and installD and installT:
            # we later add installD again, so substract here add
            # installT(otal) to get to the actual end time including
            # waiting for lock and node start up
            res[platform]['install']['timestamp'] = startT - installD + installT
            res[platform]['install']['duration'] = installD
            res[platform]['install']['install_duration'] = installD
            res[platform]['install']['total_duration'] = installT

        if startT and installD and viewD and viewT:
            # we later add installD and viewD again, so substract here
            # add viewT(otal) to get to the actual end time including
            # waiting for lock and node start up
            res[platform]['install']['timestamp'] = startT - installD - viewD + viewT
            # total time we actually spent on the nodes
            res[platform]['install']['duration'] = installD + viewD
            res[platform]['install']['install_duration'] = installD
            res[platform]['install']['view_duration'] = viewD
            res[platform]['install']['total_duration'] = installT + viewT

        for step in ('build', 'post_build', 'install', 'test', 'testi', 'testsh'):
            if step not in res[platform]:
                if run['building']:
                    res[platform][step] = {'result': 'SCHEDULED'}
                else:
                    res[platform][step] = {'result': 'ERROR'}

        if 'build' in res[platform]:
            res[platform]['build']['build_id'] = matches.get('build_id', None)
            try:
                if not isinstance(matches['test'], str):
                    matches['test'] = matches['test'][0]

                if int(matches['test']) != 100 and \
                        res[platform]['build']['result'] not in ('ERROR', 'FAILURE', 'ABORTED'):
                    res[platform]['build']['result'] = 'UNSTABLE'
            except (IndexError, KeyError):
                logger.debug("Can't get test result percentage, probably not complete")
                _result = res[platform]['build'].get('result', '?')
                _icon = res[platform]['build'].get('iconColor', '?')
                logger.debug(f"Url: {pipeline_build['url']}, Result: {_result}, Icon: {_icon}")
                pass

            # res[platform]['build']['timestamp'] = latest_build['timestamp']
            try:
                res[platform]['build']['hostname'] = matches["hostname"]
            except KeyError:
                logger.error("Failed to get hostname from log: " + "{0}consoleText".format(pipeline_build['url']))
                res[platform]['build']['hostname'] = 'Unknown'

    return res


def main():
    def check_timestamps(release, platform):
        platform = platform.replace("alma", "el")
        result = 'ERROR'
        weekday = datetime.date.today().strftime('%a')

        filename = os.path.join(CVMFS_REPOSITORY, release, weekday, STABLE_PREFIX + platform)
        logger.debug("Checking isDone file {0}".format(filename))
        if os.path.exists(filename):
            logger.debug("isDone file exists")
            timestamp = datetime.date.fromtimestamp(os.path.getmtime(filename))
            if timestamp == datetime.date.today():
                result = 'SUCCESS'
            else:
                result = 'FAILURE'

        filename = os.path.join(CVMFS_REPOSITORY, release, weekday, UNSTABLE_PREFIX + platform)
        logger.debug("Checking isDone-unstable file {0}".format(filename))
        if os.path.exists(filename):
            logger.debug("isDone-unstable file exists")
            timestamp = datetime.date.fromtimestamp(os.path.getmtime(filename))
            if timestamp == datetime.date.today():
                result = 'UNSTABLE'
            else:
                result = 'FAILURE'

        logger.debug(f'Final status: {platform} -> {result}')
        return result

    def print_dev(release, result):
        timestamps = {}
        if result:
            for platform in sorted(result.keys()):
                timestamps[platform] = check_timestamps(release, platform)
        html_engine.print_section(release, result, "dev", timestamps)

    def print_pipeline(release, result):
        timestamps = {}
        if result:
            for platform in sorted(result.keys()):
                timestamps[platform] = check_timestamps(release, platform)
        html_engine.print_section(release, result, "pipeline", timestamps)

    html_engine = HTML("report/index.html", "Nightly builds status")
    html_engine.print_page_title_jenkins()

    for toolchain in ('dev3', 'dev4', 'dev3cuda', 'dev4cuda', 'devswan','devnxcals', 'devAdePT', 'dev3python312', 'dev3lhcb', 'dev4lhcb', 'devship', 'devkey', 'devkey-head'):
        # build_info = do_matrix(f"lcg_ext_{toolchain}_archdocker_nonblocking", "dev")
        logger.info("RELEASE: {0}".format(toolchain))
        build_info = do_matrix_pipeline('lcg_nightly_general_matrix', "dev", toolchain)
        print_pipeline(toolchain, build_info)

    build_info = do_matrix_pipeline('lcg_nightly_special_matrix', "dev", "devgeantv")
    print_pipeline("devgeantv", build_info)

    build_info = do_matrix("jenkins-test-docker", 'experimental')
    html_engine.print_section("Docker smoke test", build_info)

    # we no longer run CVMFS cleaning every day, part of CVMFS publication step now
    # build_info = do_matrix("lcg_clean_cvmfs_nightlies", "clean")
    # html_engine.print_section("CVMFS cleaning", build_info)
    return 0


if __name__ == '__main__':
    time_start = datetime.datetime.now()
    init()
    setup_logging(logging.INFO, logging.WARN, logging.INFO, None)
    main()
    time_delta = datetime.datetime.now() - time_start
    print(f"Done in {time_delta}!")
