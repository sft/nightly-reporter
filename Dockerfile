FROM python:3

# Create workspace
ENV INSTALL_PATH /opt/reporter
RUN mkdir -p $INSTALL_PATH
WORKDIR $INSTALL_PATH

# Install python dependencies
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Set timezone to CET
RUN ln -sf /usr/share/zoneinfo/CET /etc/localtime

CMD [ "python" ]
