#!/usr/bin/env bash
set +x # Avoid debug prints
set -e # Quit on error

DOCKER_TAG=20220823

# Open SSH tunnel to Jenkins
SSH_SERVER="spi-jenkins-02"
SSH_CONTROL_PATH="./sftnight_${SSH_SERVER}_22"
ssh -f -N -M -S ${SSH_CONTROL_PATH} -L 8080:localhost:8080 jenkins@${SSH_SERVER}

# Get the latest Docker image of the build environment
docker pull gitlab-registry.cern.ch/sft/nightly-reporter:${DOCKER_TAG}

# Run the script using the host network to provide the SSH tunnel
docker run --rm --net="host" \
    -v "${PWD}":/opt/reporter \
    -v /cvmfs:/cvmfs \
    gitlab-registry.cern.ch/sft/nightly-reporter:${DOCKER_TAG} \
    python -B get_jenkins.py

docker run --rm --net="host" \
    -v "${PWD}":/opt/reporter \
    -v /cvmfs:/cvmfs \
    gitlab-registry.cern.ch/sft/nightly-reporter:${DOCKER_TAG} \
    python -B get_jira.py


# Close SSH tunnel to Jenkins
ssh -S ${SSH_CONTROL_PATH} -O exit ${SSH_SERVER}

# Copy report to EOS
export EOS_MGM_URL=root://eosuser.cern.ch
LCGDOCS_PATH="/eos/project/l/lcgdocs/www"

if [ -s "report/index.html" ]
then
    echo "Synchronizing report ..."
    rsync -rlpgoDvhz --checksum --delete ${WORKSPACE}/report ${LCGDOCS_PATH}
    # preserve an archive to monitor historical data, one file per day
    rsync -rlpgoDvhz --checksum ${WORKSPACE}/report/index.html ${LCGDOCS_PATH}/report_archive/index_$(date +%Y%m%d).html
fi
