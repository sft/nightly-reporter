import datetime
import logging
import os
import sys
from collections import defaultdict
from time import sleep

from html_engine import HTML

import colorlog
import requests

MAX_RETRIES = 10  # Maximum number of retries before giving up
RETRY_TIMEOUT = 15  # Time (s) between retries
JIRA_SERVER = "https://its.cern.ch/jira/"
JIRA_SEARCH_URL = JIRA_SERVER + "rest/api/2/search?"
JIRA_BROWSE_URL = JIRA_SERVER + 'browse/'
JIRA_TOKEN = os.environ.get("JIRA_BEARER_TOKEN", "")
EOS_URL = "https://lcgdocs.web.cern.ch/lcgdocs/report/"
logger = None


def setup_logging(level=logging.DEBUG):
    global logger

    logger = logging.getLogger('main')
    logger.setLevel(level)

    # create console handler with a higher log level
    ch1 = logging.StreamHandler(sys.stdout)
    ch1.setLevel(level)

    # create formatter and add it to the handlers
    # formatter1 = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    if colorlog:
        ch1.setFormatter(
            colorlog.ColoredFormatter(
                '%(asctime)s.%(msecs)03d %(log_color)s[%(name)s:%(levelname)s]%(reset)s %(message)s',
                datefmt='%H:%M:%S'))
    else:
        ch1.setFormatter(logging.Formatter(fmt="%(asctime)s.%(msecs)03d [%(name)s:%(levelname)s] %(message)s",
                                           datefmt='%H:%M:%S'))
    # add the handlers to the logger
    logger.addHandler(ch1)


def get(url):
    retry = 1
    while retry <= MAX_RETRIES:
        try:
            headers = {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": f"Bearer {JIRA_TOKEN}",
            }
            r = requests.get(url, headers=headers)
            r.raise_for_status()
        except requests.RequestException as e:
            logger.error("Request for {0} failed: {1}, {2}".format(url, str(e),
                                                                   "retrying" if retry != MAX_RETRIES else "aborting!"))
            retry += 1
            sleep(RETRY_TIMEOUT)
        else:
            return r

    logging.fatal("Failed to retrieve {0} in {1} tries".format(url, MAX_RETRIES))
    exit(1)


def search_jira(jql, max_count=1000, fields=None):
    fields = fields or ['created']
    return get(JIRA_SEARCH_URL + 'jql=' + jql + '&maxResults=' + str(max_count) + '&fields=' + ','.join(fields))


def do_jira(html):
    def parse_date(d):
        return datetime.datetime.strptime(d, '%Y-%m-%dT%H:%M:%S.%f%z')

    def format_date(d):
        return datetime.datetime.strptime(d, '%Y-%m-%dT%H:%M:%S.%f%z').strftime('%d.%m.%Y')

    def format_icon(i, size='100%'):
        return f"<img src={i['iconUrl']} alt={i['name']} style='width:{size}'>"

    def format_issue(t):
        return f"<li> {format_icon(t['fields']['issuetype'], '16px')} {format_icon(t['fields']['priority'], '16px')} " \
               f"<a href='{JIRA_BROWSE_URL}{t['key']}'>{t['key']}</a> \"{t['fields']['summary'].strip()}\""

    ticket_fields = ('issuetype', 'created', 'priority', 'status', 'summary', 'creator', 'reporter', 'assignee')

    assignees = defaultdict(list)

    # First, look for unassigned JIRAs
    r = search_jira('project=SPI+AND+resolution=Unresolved+AND+assignee%20in%20(EMPTY)+ORDER+BY+created+DESC',
                    fields=ticket_fields)

    # print("<h3>Open unassigned tickets</h3>")
    html.print_section_start("Unassigned tickets")
    html.print_raw(("<ul style=\"font-size: medium; list-style-type: none;\">",))
    html.print_raw(['<li>' + format_issue(ticket) + f" (created {format_date(ticket['fields']['created'])})</li>"
                    for ticket in r.json()['issues']])
    html.print_raw(("</ul>",))

    html.print_section_end()
    html.print_section_start("Recent tickets")
    html.print_raw(('<ul style="font-size: medium; list-style-type: none;">',))

    # Then, look for new JIRAs
    today = datetime.date.today()
    yesterday = (today - datetime.timedelta(hours=24))

    r = search_jira(f'project=SPI+AND+resolution=Unresolved',  # +AND+created>={yesterday_str}+AND+created<={today_str}
                    fields=ticket_fields)
    for ticket in r.json()['issues']:
        try:
            assignee = f"{ticket['fields']['assignee']['displayName']}"
            # f"<img src=\"{ticket['fields']['assignee']['avatarUrls']['16x16']}\">" \
            assignees[assignee].append(ticket)
        except TypeError:
            continue

        created = parse_date(ticket['fields']['created'])
        if created.date() == today or created.date() == yesterday:
            html.print_raw((f"<li>{format_issue(ticket)}, created {format_date(ticket['fields']['created'])}, "
                           f"assigned to {assignee}</li>",))

    html.print_raw(("</ul>",))
    html.print_section_end()
    html.print_section_start("Reminder of tickets per person")

    if assignees.items():
        for person, tickets in assignees.items():
            html.print_raw((f"<h3>{person}</h3>", "<ul style=\"font-size: medium; list-style-type: none;\">"))
            html.print_raw(['<li>' + format_issue(ticket) + '</li>' for ticket in tickets])
            html.print_raw(["</ul>"])
    else:
        html.print_raw(["Nothing to display"])


if __name__ == '__main__':
    setup_logging(logging.INFO)
    html_engine = HTML("report/jira.html", "JIRA report")
    html_engine.print_page_title_jira()
    do_jira(html_engine)
    html_engine.print_html_end()
