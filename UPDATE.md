# Updating nightly reporter

The nightly reporter script has to be updated every time the Jenkins pipeline changes or a list of nightly builds changes. 

In SPI, there are two types of nightly jobs that are monitored: pipeline (e.g. lcg_nightly_pipeline) and freeflow (e.g. jenkins-test-docker),

For freeflow jobs, one has to manually keep track of the entire chain of triggered jobs - see `do_matrix` function for example.

For pipeline jobs, all steps from choosing host to testing final installation are done in a single job. Since there is no simple way of requesting the log only for important stages, the nightly reporter script parses the entire log, looking for special markers of pipeline stages (`parse_pipeline_log`). Once the log file is split into stages, the `analyze_pipeline_log` function takes care of mapping pipeline stage names to phases (build, install, 3 types of tests). If the pipeline definition changes, the mapping inside that function may need to be changed, otherwise the status and/or duration of some stages may be reported incorrectly.

The easiest way to figure out new stage names is to dowload the full build log locally and analyze it with a script like this:

```py3
import logging

from get_jenkins import parse_pipeline_log, setup_logging
from pathlib import Path


def main():
    setup_logging(logging.INFO, logging.WARN, logging.INFO, None)
    with Path('consoleText').expanduser().open('r') as f:
        res = parse_pipeline_log(f.readlines())
        for k, v in res.items():
            print(f'{k}: {len(v)}')
    pass


if __name__ == '__main__':
    main()

```

The output will look like this:

```
withCredentials.withEnv.timestamps.(Environment).node: 3
withCredentials.withEnv.timestamps.(Environment).node.checkout: 14
withCredentials.withEnv.timestamps.(InDocker).node: 3
withCredentials.withEnv.timestamps.(InDocker).node.checkout: 5
withCredentials.withEnv.timestamps.(InDocker).node.withEnv.withEnv.sh: 6
withCredentials.withEnv.timestamps.(InDocker).node.withEnv.withEnv.sh#0: 11
withCredentials.withEnv.timestamps.(InDocker).node.withEnv.withDockerContainer: 3
withCredentials.withEnv.timestamps.(InDocker).node.withEnv.withDockerContainer.(Build).script.catchError.sh: 3238
withCredentials.withEnv.timestamps.(InDocker).node.withEnv.withDockerContainer.(Build).script.sh: 1
withCredentials.withEnv.timestamps.(InDocker).node.withEnv.withDockerContainer.(Build).script.sh#0: 1
withCredentials.withEnv.timestamps.(InDocker).node.withEnv.withDockerContainer.(Build).script.sh#1: 2
withCredentials.withEnv.timestamps.(InDocker).node.withEnv.withDockerContainer.(CopyToEOS).script.sh: 141
withCredentials.withEnv.timestamps.(InDocker).node.withEnv.withDockerContainer#0: 2
withCredentials.withEnv.timestamps.(CVMFSInstallation).(CVMFSInstall).node: 3
withCredentials.withEnv.timestamps.(CVMFSInstallation).(CVMFSInstall).node.checkout: 5
withCredentials.withEnv.timestamps.(CVMFSInstallation).(CVMFSInstall).node.withEnv.script.sh: 10542
withCredentials.withEnv.timestamps.(CVMFSInstallation).(CVMFSInstall).node.withEnv.script.sh#0: 1
withCredentials.withEnv.timestamps.(TestInDocker).node: 3
withCredentials.withEnv.timestamps.(TestInDocker).node.checkout: 5
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withEnv.sh: 15
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withEnv.sh#0: 2
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer: 3
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(WaitForCVMFS).script.sh: 23
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestWithViews).script.dir: 1
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestWithViews).script.dir.git: 20
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestWithViews).script.sh: 2687
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestShell).script.dir: 1
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestShell).script.dir.git: 20
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestShell).script.sh: 47
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestPythonImport).script.dir: 1
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestPythonImport).script.dir.git: 20
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestPythonImport).script.sh: 2636
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer#0: 2
```

Usually the important stages have several thousands lines of log (the number after stage name in the output above):
```
withCredentials.withEnv.timestamps.(InDocker).node.withEnv.withDockerContainer.(Build).script.catchError.sh: 3238
withCredentials.withEnv.timestamps.(CVMFSInstallation).(CVMFSInstall).node.withEnv.script.sh: 10542
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestWithViews).script.sh: 2687
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestShell).script.sh: 47
withCredentials.withEnv.timestamps.(TestInDocker).node.withEnv.withDockerContainer.(TestPythonImport).script.sh: 2636
```

(one notable exception is the `TestShell` phase, which is simpler than the others and doesn't produce as much output).

After figuring out the new stage name one should update `stage_map` mapping inside `analyze_pipeline_log` and rebuild docker image.
