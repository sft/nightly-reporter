from __future__ import print_function

import base64
import datetime
from collections import defaultdict


class HTML:
    """This class help to format the HTML tags"""

    def __init__(self, filename, title):
        self.filename = filename
        self.icons = defaultdict(lambda: "✅")

        self.icons['SCHEDULED'] = "⌛"
        self.icons['UNSTABLE'] = "⚠️"
        self.icons['ERROR'] = "🚫"
        self.icons['FAILURE'] = "📛"
        self.icons['ABORTED'] = "🗑️" # not used
        self.icons[None] = "▶️"

        self.print_html_start(title)

    def print_raw(self, lines):
        with open(self.filename, "a") as output:
            for line in lines:
                print(line, file=output)

    def inline_img(self, iconPath):
        """inline image with base64"""

        with open(iconPath, "rb") as icon:
            encoded = base64.b64encode(icon.read()).decode("utf-8")

        extension = iconPath.split(".")[-1]

        header = '"data:image/' + extension + ';base64, '

        trailer = '"' # base64.b64encode adds trailing '=='

        output = header + encoded + trailer

        return output

    def inline_css(self, stylesheetPath):
        """translates css file contents to a <style> block"""

        with open(stylesheetPath, "r") as css:
            output = "<style>"
            output += css.read()
            output += "</style>"

        return output

    def add_grafana_link(self, build_node):
        """Turn the build node into a link to the grafana."""
        if build_node.startswith("mac"):
            return build_node
        arch = "x86_64" if "x86" in build_node else "aarch64"
        return f'<a href="https://monit-grafana.cern.ch/d/sY3GKbPVz/spi-hosts-monitor?from=now-24h&orgId=59&to=now&var-environment=All&var-hostgroup=lcgapp/build/{arch}/c8&var-hostname={build_node}.cern.ch&var-rp=one_week&var-bin=1m" >{build_node}</a>'

    def print_html_start(self, title):
        with open(self.filename, "w") as output:
            print("<!DOCTYPE html>", file=output)
            print("<html lang=\"en\">", file=output)
            print("<head>", file=output)
            print('    <meta charset="utf-8" />', file=output)
            print('    <meta http-equiv="refresh" content="300">', file=output)
            print('    <meta http-equiv="x-ua-compatible" content="ie=edge">', file=output)
            print('    <meta name="viewport" content="width=device-width, initial-scale=1">', file=output)
            print('    <base href="." target="_blank">', file=output)
            print(f"    <title>{title}</title>", file=output)
            print(self.inline_css("./report/css/bootstrap.min.css"), file=output)
            print(self.inline_css("./report/css/report.css"), file=output)
            print(self.inline_css("./report/css/cdash-icon.css"), file=output)
            print("</head>", file=output)
            print("<body>", file=output)
            print("<div class=\"container-fluid\">", file=output)

    def print_html_end(self):
        with open(self.filename, "a") as output:
            print("</div>", file=output)
            print("</body>", file=output)
            print("</html>", file=output)

    def print_page_title_jenkins(self):
        with open(self.filename, "a") as output:
            now = datetime.datetime.now()
            date = now.strftime("%Y-%m-%d")
            time = now.strftime("%H:%M")
            print('<div class="page-header">', file=output)
            print("    <h1>Nightly build report for {0} <small>(generated at {1})</small></h1>".format(date, time),
                  file=output)
            print('    <a class="btn btn-primary pull-left"', file=output)
            print('       href="jira.html"', file=output)
            print('       style="margin-top: -30px;" target="_top">JIRA report</a>', file=output)
            print('    <a class="btn btn-primary pull-right"', file=output)
            print('       href="https://lcgapp-services.cern.ch/cdash/index.php?project=LCGSoft"', file=output)
            print('       style="margin-top: -30px;">CDash</a>', file=output)
            print("</div>", file=output)
            # Display warning (invisible on load)
            print('<div id="warning" style="font-size: 22px; color: #000; display: none" class="alert alert-warning">',
                  file=output)
            print(f'<span style="margin-right: 25px">{self.icons["UNSTABLE"]}</span>', file=output)
            print("    <strong>WARNING!</strong>", file=output)
            print("    <span>This report is older than 3 hours.</span>", file=output)
            print("    <a href='https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_nightly_reporter/'>" +
                  "Check Jenkins log for possible issues</a></span>", file=output)
            print(f'<span style="margin-left: 25px; float:right">{self.icons["UNSTABLE"]}</span>', file=output)
            print("</div>", file=output)
            # Trigger to make warning visible for reports older than three hours
            print("<script>", file=output)
            print("if( ((Date.now() - Date.parse('{0} {1}')) / 3600000) > 3 ) {{".format(date, time), file=output)
            print('    document.getElementById("warning").style.display="block";', file=output)
            print("}", file=output)
            print("</script>", file=output)

    def print_page_title_jira(self):
        with open(self.filename, "a") as output:
            now = datetime.datetime.now()
            date = now.strftime("%Y-%m-%d")
            time = now.strftime("%H:%M")
            print('<div class="page-header">', file=output)
            print("    <h1>JIRA report for {0}</h1>".format(date), file=output)
            print('    <a class="btn btn-primary pull-left"', file=output)
            print('       href="index.html"', file=output)
            print('       style="margin-top: -30px;" target="_top">Jenkins report</a>', file=output)
            print('    <a class="btn btn-primary pull-right"', file=output)
            print('       href="https://sft.its.cern.ch/jira"', file=output)
            print('       style="margin-top: -30px;">SPI JIRA</a>', file=output)
            print("</div>", file=output)

    def print_section_start(self, headline):
        with open(self.filename, "a") as output:
            print('<div class="panel panel-default">', file=output)
            print('<div class="panel-body">', file=output)
            print("<a href=\"#{0}\"><h2 id=\"{0}\">{0}</h2></a>".format(headline), file=output)

    def print_section_end(self):
        with open(self.filename, "a") as output:
            print("</div>", file=output)
            print("</div>", file=output)

    def print_table_start(self):
        with open(self.filename, "a") as output:
            print('<table class="table table-striped table-hover">', file=output)
            print("<tbody>", file=output)

    def print_table_end(self):
        with open(self.filename, "a") as output:
            print("</tbody>", file=output)
            print("</table>", file=output)

    @staticmethod
    def format_duration(duration):
        if duration is None:
            return "No Info"
        if isinstance(duration, datetime.timedelta):
            dur_ = duration.total_seconds()
        else:
            dur_ = duration

        return str(datetime.timedelta(seconds=round(dur_)))

    def print_dev(self, result, is_geantv, is_cuda, timestamps):
        def make_console_link(base):
            if is_cuda:
                return result[platform][step]['url'].rstrip('/') + '/log/?consoleFull'
            else:
                return result[platform][step]['url'].rstrip('/') + '/consoleFull'

        with open(self.filename, "a") as output:
            # Table head
            print("<tr>", file=output)
            print("    <th>Platform</th>", file=output)
            print("    <th>Started</th>", file=output)
            print('    <th colspan="3">Build</th>', file=output)
            if not (is_geantv or is_cuda):
                print('    <th class="separated">Post-build</th>', file=output)
            print('    <th class="separated" colspan="2">Install</th>', file=output)
            if not is_geantv:
                print('    <th class="separated" colspan="3">Test</th>', file=output)
            print('    <th class="separated">Completed</th>', file=output)
            print("    <th>isDone file</th>", file=output)
            print("</tr>", file=output)
            print("<tr>", file=output)
            print("    <th></th>", file=output)  # Platform
            print("    <th></th>", file=output)  # Started at
            print("    <th>Node</th>", file=output)  # Build (1/3)
            print("    <th>Duration</th>", file=output)  # Build (2/3)
            print("    <th>Result+Log</th>", file=output)  # Build (3/3)
            if not (is_geantv or is_cuda):
                print('    <th class="separated">Status</th>', file=output)  # Post-build
            print('    <th class="separated">Duration</th>', file=output)  # Install (1/2)
            print("    <th>Status+Log</th>", file=output)  # Install (2/2)
            if not is_geantv:
                print('    <th class="separated">lcgtest</th>', file=output)  # Test (1/3)
                print("    <th>import</th>", file=output)  # Test (2/3)
                print("    <th>shell</th>", file=output)  # Test (2/2)
            print('    <th class="separated"></th>', file=output)  # Completed
            print("    <th>timestamp</th>", file=output)  # isDone
            print("</tr>", file=output)
            # Table body
            for platform in sorted(result.keys()):
                print("<tr>", file=output)
                print("    <td>{0}</td>".format(platform), file=output)

                if 'build' not in result[platform] or result[platform]['build']['result'] == 'SCHEDULED':
                    print("    <td>No data</td>", file=output)
                else:
                    try:
                        started = result[platform]['build']['timestamp'].strftime("%H:%M:%S")
                    except (KeyError, AttributeError):
                        started = "No data"

                    print("    <td>{0}</td>".format(started), file=output)

                span = {'build': 2, 'post_build': 1, 'install': 2, 'test': 1, 'testi': 1,
                        'testsh': 1}
                for step in ('build', 'post_build', 'install', 'test', 'testi', 'testsh'):

                    # Skip tests in GeantV
                    if is_geantv and step in ('test', 'testi', 'testsh'):
                        continue

                    # Skip post-build for cuda/geantv builds
                    if (is_geantv or is_cuda) and step == 'post_build':
                        continue

                    if step not in result[platform]:
                        print('<td class="separated" colspan="{0}">{1}</td>'
                                .format(span[step], self.icons['ERROR']),
                              file=output)
                    else:
                        if step == 'build':
                            build_node = self.add_grafana_link(result[platform][step]['hostname'])
                            print("    <td>{0}</td>".format(build_node), file=output)

                        step_result = result[platform][step]['result']
                        step_icon = self.icons[step_result]
                        if step_result in ('SCHEDULED', 'ERROR'):
                            # Show icon on both columns if job is scheduled or running
                            print('    <td class="separated" colspan="{0}">{1}</td>'.format(
                                span[step], step_icon), file=output)
                        else:
                            step_url = make_console_link(result[platform][step]['url'])
                            if step in ('post_build', 'test', 'testi', 'testsh'):
                                print('    <td class="separated"><a href="{0}">{1}</a></td>'.format(
                                    step_url, step_icon), file=output)
                            else:
                                try:
                                    step_duration = result[platform][step]['duration']
                                    formatted_duration = self.format_duration(step_duration)
                                except KeyError as e:
                                    formatted_duration = "???"
                                    print("No duration! Platform {0}, step {1}".format(platform, step))
                                if step == 'install':
                                    totalD = self.format_duration(result[platform]['install'].get('total_duration'))
                                    instaD = self.format_duration(result[platform]['install'].get('install_duration'))
                                    viewsD = self.format_duration(result[platform]['install'].get('view_duration'))
                                    timeInfo = f"Total: {totalD}, install: {instaD}, view: {viewsD}"
                                    print(f'    <td class="separated" title="{timeInfo}">{formatted_duration}</td>', file=output)
                                else:
                                    print('    <td class="separated">{0}</td>'.format(formatted_duration), file=output)

                                if result[platform][step].get('build_id', None) is None:
                                    cdash_extra = ''
                                else:
                                    cdash_extra = '&nbsp;<a href="https://lcgapp-services.cern.ch/cdash/buildSummary.php?buildid={0}">' + \
                                                  '<span class="cdash-icon" alt="CDash build details" /></a>&nbsp;' + \
                                                  '<a href="https://lcgapp-services.cern.ch/cdash/viewFiles.php?buildid={0}">' + \
                                                  '<span title="Log files (if any)">📦</span></a>&nbsp;'
                                    cdash_extra = cdash_extra.format(result[platform][step].get('build_id'))

                                print(
                                    '    <td><a href="{0}">{1}</a>{2}</td>'.format(step_url, step_icon,
                                                                                                 cdash_extra),
                                    file=output)

                if 'install' in result[platform]:
                    install_result = result[platform]['install']['result']
                    if install_result in ('SCHEDULED', None):
                        print('    <td class="separated" colspan="2">{0}</td>'.format(
                            self.icons['SCHEDULED']), file=output)
                    elif install_result == 'ERROR':
                        print('    <td class="separated" colspan="2">{0}</td>'.format(
                            self.icons['ERROR']), file=output)
                    else:
                        install_timestamp = result[platform]['install']['timestamp']
                        install_duration = result[platform]['install']['duration']
                        if not isinstance(install_duration, datetime.timedelta):
                            install_duration = datetime.timedelta(seconds=install_duration)
                        if isinstance(install_timestamp, datetime.datetime):
                            end_time = install_timestamp + install_duration
                            completed = end_time.strftime("%H:%M:%S")
                        else:
                            completed = 'No data'
                        print(f'    <td class="separated">{completed}</td>', file=output)

                        if install_result in ('SUCCESS', 'UNSTABLE'):
                            timestamp = timestamps[platform]
                            icon = self.icons[timestamp]
                            print('    <td>{0}</td>'.format(icon), file=output)
                        else:
                            print('    <td>{0}</td>'.format(self.icons['ERROR']), file=output)
                else:
                    print('    <td class="separated" colspan="2">{0}</td>'.format(
                        self.icons['SCHEDULED']), file=output)

                print("</tr>", file=output)

    def print_exp(self, result):
        with open(self.filename, "a") as output:
            # Table head
            print("<tr>", file=output)
            print("    <th>Platform</th>", file=output)
            print("    <th>Started</th>", file=output)
            print('    <th colspan="3">Build</th>', file=output)
            print("    <th>Completed</th>", file=output)
            print("</tr>", file=output)
            print("<tr>", file=output)
            print("    <th></th>", file=output)
            print("    <th></th>", file=output)
            print("    <th>Node</th>", file=output)
            print("    <th>Duration</th>", file=output)
            print("    <th>Result+Log</th>", file=output)
            print("    <th></th>", file=output)
            print("</tr>", file=output)
            # Table body
            for platform in sorted(result.keys()):
                print("<tr>", file=output)
                print("    <td>{0}</td>".format(platform), file=output)

                if 'build' in result[platform]:
                    scheduled = result[platform]['build']['result'] == 'SCHEDULED'

                    if scheduled:
                        print("    <td>No data</td>", file=output)
                    else:
                        try:
                            started = result[platform]['build']['timestamp'].strftime("%H:%M:%S")
                        except (KeyError, AttributeError):
                            started = "No data"
                        print("    <td>{0}</td>".format(started), file=output)

                    build_node = result[platform]['build']['hostname']
                    print("    <td>{0}</td>".format(build_node), file=output)

                    build_duration = result[platform]['build']['duration']
                    formatted_duration = self.format_duration(build_duration)
                    print(f"    <td>{formatted_duration}</td>", file=output)

                    build_result = self.icons[result[platform]['build']['result']]
                    if scheduled:
                        print('    <td>{0}</td>'.format(build_result), file=output)
                        print('    <td class="separated">{0}</td>'.format(build_result), file=output)
                    else:
                        build_url = result[platform]['build']['url'] + "consoleFull"
                        print('    <td><a href="{0}">{1}</a></td>'.format(build_url, build_result),
                              file=output)

                        build_timestamp = result[platform]['build']['timestamp']
                        end_time = build_timestamp + datetime.timedelta(seconds=build_duration)
                        completed = end_time.strftime("%H:%M:%S")
                        print('    <td class="separated">{0}</td>'.format(completed), file=output)
                else:
                    print('    <td colspan="5">{0}</td>'.format(self.icons['ERROR']), file=output)

                print("</tr>", file=output)

    def print_section(self, headline, result, build_type="exp", timestamps=None):
        timestamps = timestamps or {}

        self.print_section_start(headline)
        if result is None:
            with open(self.filename, 'a') as output:
                print("Not built?", file=output)
        else:
            self.print_table_start()
            if build_type in {'dev', 'pipeline'}:
                is_geantv = False  # headline.lower() == "devgeantv"
                is_pipeline = build_type == 'pipeline'
                self.print_dev(result, is_geantv, is_pipeline, timestamps)
            else:
                self.print_exp(result)
            self.print_table_end()
        self.print_section_end()
